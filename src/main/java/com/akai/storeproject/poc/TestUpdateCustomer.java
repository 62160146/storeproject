/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.akai.storeproject.poc;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;

/**
 *
 * @author ii drunkboy
 */
public class TestUpdateCustomer {
   public static void main(String[] args) {
   Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
   try {
       String sql = "UPDATE customer SET name = ?, tel = ? WHERE id = ?";
       PreparedStatement stmt = conn.prepareStatement(sql);
       Customer customer = new Customer(2,"Jennie Kim","0612306500");
       stmt.setString(1, customer.getName());
       stmt.setString(2, customer.getTel());
       stmt.setInt(3, customer.getId());
       int row = stmt.executeUpdate();
       System.out.println("Affect row "+ row);
   } catch (SQLException ex) {
    Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
      }
        db.close();
    }
}

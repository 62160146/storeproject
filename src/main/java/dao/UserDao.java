/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.akai.storeproject.poc.TestSelectProduct;
import com.akai.storeproject.poc.TestSelectUser;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;

/**
 *
 * @author ii drunkboy
 */
public class UserDao implements DaoInterface<User> {

    @Override
    public int add(User object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;

   try {
    String sql = "INSERT INTO user (name, tel, password) VALUES (?,?,?)";
       PreparedStatement stmt = conn.prepareStatement(sql);
       stmt.setString(1, object.getName());
       stmt.setString(2, object.getTel());
       stmt.setString(3, object.getPassword());
       int row = stmt.executeUpdate();
       ResultSet result = stmt.getGeneratedKeys();
       if(result.next()){
           id = result.getInt(1);
       }
   } catch (SQLException ex) {
    Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
      }
         db.close();
         return id;
    }

    @Override
    public ArrayList<User> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,tel,password FROM user";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                String password = result.getString("password");
                User user = new User(id,name,tel,password);
                list.add(user);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectUser.class.getName()).log(Level.SEVERE, null, ex);
        }  
        db.close();
        return list;
    }

    @Override
    public User get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,tel,password FROM user";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int uid = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                String password = result.getString("password");
                User user = new User(uid,name,tel,password);
                return user;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectUser.class.getName()).log(Level.SEVERE, null, ex);
        }  
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
   try {
       String sql = "DELETE FROM user WHERE id = ?";
       PreparedStatement stmt = conn.prepareStatement(sql);
       stmt.setInt(1, id);
       row = stmt.executeUpdate();
   } catch (SQLException ex) {
    Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
      }
        db.close(); 
        return row;
    }

    @Override
    public int update(User object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
   try {
       String sql = "UPDATE user SET name = ?, tel = ?, password = ? WHERE id = ?";
       PreparedStatement stmt = conn.prepareStatement(sql);
       stmt.setString(1, object.getName());
       stmt.setString(2, object.getTel());
       stmt.setString(3, object.getPassword());
       stmt.setInt(4, object.getId());
       row = stmt.executeUpdate();
       System.out.println("Affect row "+ row);
   } catch (SQLException ex) {
    Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
      }
        db.close();
        return row;
    }

    public static void main(String[] args) {
        UserDao dao = new UserDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new User(-1,"Jennie","0612306501","paaword"));
        System.out.println("id: "+id);
        User lastUser = dao.get(id);
        System.out.println("Last User: "+lastUser);
        lastUser.setPassword("12122003");
        int row = dao.update(lastUser);
        User updateUser = dao.get(id);
        System.out.println("Update User: "+updateUser);
        dao.delete(id);
        User deleteUser = dao.get(id);
        System.out.println("Delete User: "+ deleteUser);
        
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.akai.storeproject.poc.TestSelectCustomer;
import com.akai.storeproject.poc.TestSelectProduct;
import com.akai.storeproject.poc.TestSelectUser;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;

/**
 *
 * @author ii drunkboy
 */
public class CustomerDao implements DaoInterface<Customer> {

    @Override
    public int add(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;

   try {
    String sql = "INSERT INTO customer (name, tel) VALUES (?,?)";
       PreparedStatement stmt = conn.prepareStatement(sql);
       Customer customer = new Customer(-1,"Jennie","0612306501");
       stmt.setString(1, object.getName());
       stmt.setString(2, object.getTel());
       int row = stmt.executeUpdate();
       ResultSet result = stmt.getGeneratedKeys();
       if(result.next()){
           id = result.getInt(1);
       }
   } catch (SQLException ex) {
    Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
      }
         db.close();
         return id;
    }

    @Override
    public ArrayList<Customer> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,tel FROM customer";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                Customer customer = new Customer(id,name,tel);
                list.add(customer);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }  
        db.close();
        return list;
    }

    @Override
    public Customer get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,tel FROM customer";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int cid = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                Customer customer = new Customer(cid,name,tel);
                return customer;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }  
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
   try {
       String sql = "DELETE FROM customer WHERE id = ?";
       PreparedStatement stmt = conn.prepareStatement(sql);
       stmt.setInt(1, id);
       row = stmt.executeUpdate();
   } catch (SQLException ex) {
    Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
      }
        db.close(); 
        return row;
    }

    @Override
    public int update(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
   try {
       String sql = "UPDATE customer SET name = ?, tel = ? WHERE id = ?";
       PreparedStatement stmt = conn.prepareStatement(sql);
       stmt.setString(1, object.getName());
       stmt.setString(2, object.getTel());
       stmt.setInt(3, object.getId());
       System.out.println("Affect row "+ row);
   } catch (SQLException ex) {
    Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
      }
        db.close();
        return row;
    }
    
    public static void main(String[] args) {
        CustomerDao dao = new CustomerDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new Customer(-1, "Jennie kim", "0615554442"));
        System.out.println("id: "+id);
        Customer lastCustomer = dao.get(id);
        System.out.println("Last Product: "+lastCustomer);
        lastCustomer.setTel("0841065084");
        int row = dao.update(lastCustomer);
        Customer updateCustomer = dao.get(id);
        System.out.println("Update Customer: "+updateCustomer);
        dao.delete(id);
        Customer deleteCustomer = dao.get(id);
        System.out.println("Delete Customer: "+ deleteCustomer);
        
    }
    
}
